class Files
  attr_reader :files

  def initialize
    @files = {}
    %w(1 2 3).each do |n|
      name = "found#{n}.txt"
      @files[name] = read_file(name)
    end
  end

  def read_file(name)
    File.read(name).gsub(' ', '')
  end
end

